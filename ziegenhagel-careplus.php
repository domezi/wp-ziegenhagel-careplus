<?php /*
 * Version: 3.5.1
 * Plugin Name: Ziegenhagel Care+
 * Plugin URI: https://www.ziegenhagel.com/services/careplus
 * Description: Professionelle Rundum-Plege Ihrer Webseite & Hosting
 * Author: Dominik Ziegenhagel
 * Author URI: https://www.ziegenhagel.com
 */

$ziegenhagel_careplus_changelog = '
3.5.0;20230225;Schon gewusst hinzugefügt
3.4.3;20230225;Use filemtime for Version to Date
3.4.2;20230225;Bei falscher API Antwort werden die Daten nicht mehr überschrieben
3.4.1;20230225;Fehler behoben, der bei Fehlenden Rechnungen einen Fehler verursachte
3.4.0;20230224;Bei Klick auf Support wird direkt eine Mail versendet
3.3.6;20230220;Fehler behoben, der bei nur einem User die Admin Console verhinderte
3.3.5;20230218;Wartungsseite repariert
3.3.4;20230217;Wartungsseite verschönert
3.3.3;20230217;Login as Funktion hinzugefügt
3.3.2;20230217;Console zeigt ChangeLog an
3.3.1;20230217;UID == 1 wird zu is_ziegenhagel_careplus_admin()
3.3.0;20230217;Console Widget für UID=1 hinzugefügt
3.2.0;20221128;Wiki hinzugefügt';

$schon_gewusst = [
    [
            "title" => "Etwas klappt nicht gleich?",
            "description" => "Kein Problem, wir helfen Ihnen gerne weiter. Klicken Sie einfach auf den \"Ziegenhagel Support\" Button in der Menüleiste, um eine Mail mit Informationen für uns zu erstellen.",
            "image" => "https://www.ziegenhagel.com/wp-content/uploads/2021/02/etwas-klappt-nicht-gleich.jpg",
    ],
    [
            "title"=>"Menü der Webseite bearbeiten",
            "description"=>"Sie können das Menü Ihrer Webseite ganz einfach bearbeiten. Klicken Sie dazu auf Design > Menüs. Dort können Sie die Menüpunkte hinzufügen, löschen und verschieben.",
            "image" => "https://www.ziegenhagel.com/wp-content/uploads/2021/02/etwas-klappt-nicht-gleich.jpg",
    ]
];

/* enable auto updates */
if (get_option("ziegenhagel_care", "Silber") != "Silber") {
    // enable auto updates
    add_filter('auto_update_theme', '__return_true');
    add_filter('auto_update_plugin', '__return_true');
    add_filter('auto_update_core', '__return_true');
    add_filter('allow_minor_auto_core_updates', '__return_true');
    add_filter('allow_major_auto_core_updates', '__return_true');
    add_filter('auto_update_translation', '__return_true');
}

if(!function_exists('wp_get_current_user')) { include(ABSPATH . "wp-includes/pluggable.php"); }// no need to add for theme functions
global $current_user_can;

if(!function_exists('ziegenhagel_mailto')) {
    function ziegenhagel_mailto() {
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        return "mailto:info@ziegenhagel.com?Subject=Frage zu Seite | ".get_bloginfo("name")."&body="."Seiten-Adresse: ".$actual_link;
    }
}
if(!function_exists('ziegenhagel_careplus_loginas')) {
    function ziegenhagel_careplus_loginas($username)
    {
        if(!current_user_can('administrator')) {
            wp_redirect(home_url());
            exit;
        }

        // Automatic login //
        $user = get_user_by('login', $username );

        // Redirect URL //
        if ( !is_wp_error( $user ) ) {
            wp_clear_auth_cookie();
            wp_set_current_user ( $user->ID );
            wp_set_auth_cookie  ( $user->ID );

            $redirect_to = user_admin_url();
            wp_safe_redirect( $redirect_to );
            exit();
        }
    }
}

if(isset($_GET['ziegenhagel_careplus_loginas'])) {
    ziegenhagel_careplus_loginas($_GET['ziegenhagel_careplus_loginas']);
}



if(!function_exists('is_ziegenhagel_careplus_admin')) {
    /**
     * Checks if the current user is the admin user.
     *
     * @return bool
     */
    function is_ziegenhagel_careplus_admin() {
        return get_current_user_id() == 1 || wp_get_current_user()->user_login == "ziegenhagel" || wp_get_current_user()->user_login == "dominik" || wp_get_current_user()->user_email == "info@ziegenhagel.com"  || wp_get_current_user()->user_email == "wp@zghl.de";
    }
}

function ziegenhagel_edit_role()
{
    $roleObject = get_role('editor');
    if (!$roleObject->has_cap('edit_theme_options')) {
        $roleObject->add_cap('edit_theme_options');
    }
}

add_action('admin_head', 'ziegenhagel_edit_role');

function ziegenhagel_plugin_add_settings_link($links)
{
    $settings_link = '<a href="https://ziegenhagel.com/services/careplus/" target="_blank">' . __('Care+ Pläne anzeigen') . '</a>';
    array_push($links, $settings_link);
    return $links;
}

$plugin = plugin_basename(__FILE__);
add_filter("plugin_action_links_$plugin", 'ziegenhagel_plugin_add_settings_link');


if (!function_exists('ziegenhagel_register_thumbnails')) {
    /**
     * Registers theme's additional thumbnail sizes.
     *
     * @return void
     * @todo Update prefix in the hook function and if statement.
     *
     * @todo Change function prefix to your textdomain.
     */
    function ziegenhagel_register_thumbnails()
    {
        add_image_size('custom-thumbnail', 800, 600, true);
    }
}
add_action('after_setup_theme', 'ziegenhagel_register_thumbnails');

if (!function_exists('ziegenhagel_wp_dashboard_console')) {
    function ziegenhagel_wp_dashboard_console()
    {
        $buttons = [
                [
                        "title"=>"Care+ Wiki-Editor",
                        "type"=>"boolean",
                        "option"=>"ziegenhagel_careplus_wiki_editor",
                        "description"=>"Schaltet den Editor für das CarePlus Wiki ein und aus.",
                        "link"=>get_admin_url()."index.php?ziegenhagel_careplus_wiki_editor=",
                ],
                [
                        "title"=>"Care+ Entwicklungs-Modus",
                        "type"=>"boolean",
                        "option"=>"ziegenhagel_dev",
                        "description"=>"Dadurch werden Dateiüberschreibungen bei Updates verhindert.",
                        "link"=>get_admin_url()."index.php?ziegenhagel_dev=",
                ],
                [
                        "title"=>"Care+ Update",
                        "type"=>"action",
                        "description"=>"Update this plugin from Git via regular auto repair / auto update.",
                        "link"=>get_admin_url()."index.php?ziegenhagel_sync=1",
                ],
                [
                        "title"=>"Disconnect from Overtime",
                        "type"=>"action",
                        "description"=>"Disconnect from Overtime and remove all Care+ data.",
                        "link"=>get_admin_url()."index.php?ziegenhagel_purge=1",
                ],
                [
                        "title"=>"Care+ Console",
                        "type"=>"boolean",
                        "option"=>"ziegenhagel_console",
                        "description"=>"Care+ Console nicht mehr anzeigen.",
                        "link"=>get_admin_url()."index.php?ziegenhagel_console=",
                ]
        ];

        // get the current user id
        $user_id = get_current_user_id();

        // get the next 3 users with id > current user id
        $users = get_users([
                "number"=>3,
                "offset"=>0,
                "orderby"=>"ID",
                "order"=>"ASC",
                "exclude"=>[$user_id]
        ]);

        // add the users to the buttons
        $logins = array();
        foreach($users as $user){
            $logins[] = [
                    "user"=>$user->user_login,
                    "link"=>get_admin_url()."index.php?ziegenhagel_careplus_loginas=".$user->user_login
            ];
        }

        // display buttons
        foreach($buttons as $index=>$button){

            // line break but not for first button

            // if its a boolean type, check if its true or false
            if($button["type"]=="boolean"){
                // set link
                $button["link"] .= !get_option($button["option"],false)?"1":"0";

                // rename "umschalten" to "ein" or "aus"
                $button["title"] .= !get_option($button["option"],false)?" aktivieren":" deaktivieren";
            }

            // display button
            echo '<div>
            <a class="button" href="'.$button["link"].'"> 
            <span style="vertical-align:sub;margin-left:-2px;margin-right:2px" class="dashicons dashicons-plugins-checked"></span>
            '.$button["title"].' </a>
            <br><small>'.$button["description"].'</small>
            </div><br>';
        }

        if(count($logins)) {
            echo "<b>Login:</b><br>";
            // display the logins
            foreach($logins as $index=>$login) {
                if($index>0) echo ", ";

                // display link
                echo '<a href="'.$login["link"].'">'.$login["user"].'</a>';
            }
        }

        // display changelog
        echo '<div style="margin-top:15px;white-space: pre-line;"><b>Changelog:</b>';
        global $ziegenhagel_careplus_changelog;

        // display the top 4 lines, separated by the first -
        $lines = explode("\n",$ziegenhagel_careplus_changelog);
        for($i=1;$i<5;$i++){
            /* format:
            3.3.1 (20230217) - Funktion is_ziegenhagel_careplus_admin() hinzugefügt, statt UID=1
3.3.0 (20230217) - Console Widget für UID=1 hinzugefügt
3.2.0 (20221128) - Car
                */
             [$version, $date, $description] = explode(";",$lines[$i]);
            echo "<div style='margin:5px 0' title='".date("d.m.y",strtotime($date))."'><b>".$version."</b> ".$description.".</div>";
        }

        echo '</div>';
    }
}
if (!function_exists('ziegenhagel_wp_dashboard_support')) {
    function ziegenhagel_wp_dashboard_support()
    {

	if(!current_user_can( 'edit_posts' )){echo "Keine Erlaubnis Support anzuzeigen.";return;}
        ?>
        <style> .welcome-panel li {
                font-size: 14px;
                display: inline-block;
                width: 49%;
            } </style>
        <div class="ziegenhagel-panel welcome-panel" style="background:none;border:none;margin:0;padding:0;">
            <?php

            if (isset($_GET["ziegenhagel_dev"])) {
                update_option("ziegenhagel_dev", boolval($_GET["ziegenhagel_dev"]));
            }

            if (isset($_GET["ziegenhagel_careplus_wiki_editor"])) {
                update_option("ziegenhagel_careplus_wiki_editor", boolval($_GET["ziegenhagel_careplus_wiki_editor"]));
            }

            if (isset($_GET["ziegenhagel_console"])) {
                update_option("ziegenhagel_console", boolval($_GET["ziegenhagel_console"]));
            }

            if (isset($_GET["ziegenhagel_sync"])) {
                ziegenhagel_do_this_hourly();
            }

            if (isset($_GET["ziegenhagel_maintenance_mode"])) { 
                update_option("ziegenhagel_maintenance_mode", $_GET["ziegenhagel_maintenance_mode"]); 
            }


            if (isset($_GET["ziegenhagel_purge"])) {

                $data = [
                    "secret"=>get_option("ziegenhagel_project_secret"),
                    "action"=>"uninstall"
                ];
                // create curl resource
                $ch = curl_init();

                // set url
                curl_setopt($ch, CURLOPT_URL, "https://api.ziegenhagel.com/wp_care.php");

                //return the transfer as a string
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,  http_build_query($data));
                curl_setopt($ch, CURLOPT_POST, 1);

                // $output contains the output string
                $output = curl_exec($ch);

                echo "Purge: ".$output;

                // close curl resource to free up system resources
                curl_close($ch);

                delete_option( 'ziegenhagel_project_secret' );
                delete_option( 'ziegenhagel_invoices' );
                delete_option( 'ziegenhagel_appointments' );
                delete_option( 'ziegenhagel_care' );
                delete_option( 'ziegenhagel_care_plans' );

            }


            if (isset($_GET["ziegenhagel_update"])) {
                // lets run update / auto repair
                ziegenhagel_care_do_update();
            }
            // generate project secret token
            if (get_option("ziegenhagel_project_secret", "") == "") {

                $url = explode("/", str_replace("https://", "", str_replace("http://", "", get_site_url())));
                $data = ["action" => "init", "url" => $url[0]];
                echo "<h2>Einrichtung</h2>";
                echo "<div style='margin-bottom:4px;background:white;padding:4px 7px;border:1px solid #ddd;border-radius:3px'>" . $url[0] . "</div>";

                if (isset($_GET["ziegenhagel_init_secret"])) {
                    // create curl resource
                    $ch = curl_init();

                    // set url
                    curl_setopt($ch, CURLOPT_URL, "https://api.ziegenhagel.com/wp_care.php");

                    //return the transfer as a string
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                    curl_setopt($ch, CURLOPT_POST, 1);

                    // $output contains the output string
                    $output = curl_exec($ch);

                    // close curl resource to free up system resources
                    curl_close($ch);

                    $success = json_decode($output)->success;

                    if (!$success) {
                        echo "<textarea style='border:1px solid #a00;width:100%'>" . $output . "</textarea>";
                    } else {
                        $secret = json_decode($output)->secret;
                        echo "Einrichtung abgeschlossen.";
                        update_option("ziegenhagel_project_secret", $secret);
                        ziegenhagel_do_this_hourly();
                    }
                } else echo " <a class='button primary' href='?ziegenhagel_init_secret'>Einrichtung abschliessen</a><br><br>";
            }
            ?>

            <h1>Nächste Schritte</h1>
            <ul>
                <li><a href="../" class="welcome-icon welcome-view-site">Webseite ansehen</a></li>
                <?php
                // finde die ID der Startseite
                $id = get_option('page_on_front');
                ?>
                <li><a href="post.php?post=<?php echo $id; ?>&action=edit" class="welcome-icon welcome-edit-page">Startseite bearbeiten</a></li>
                <li><a href="nav-menus.php" class="welcome-icon welcome-menus">Menüs verwalten</a></li>
                <li><a href="post-new.php?post_type=page" class="welcome-icon welcome-add-page">Seite hinzufügen</a>
                </li>
            </ul>
            <hr/>
            <form method="get" style="display:flex;align-items:center;gap:.5rem">
                <select name="ziegenhagel_maintenance_mode" onchange="this.form.submit()" style="width:100%">
                <?php
                            foreach(["off"=>"Webseite ist öffentlich zugänglich","maintenance"=>"Besuchern einen Wartungmodus anzeigen","coming_soon"=>"Entwicklungsmodus einschalten"] as $mode=>$title) {
                                    echo "<option value='".$mode."' ";
                                    if($mode == get_option("ziegenhagel_maintenance_mode"))
                                            echo "selected";
                                    echo">".$title."</option>";
                            }
                ?>
                </select>
            </form>
            <hr/>
            <a href="https://ziegenhagel.com/support#wordpress-guide" target="_blank" class="button button-primary"><span aria-hidden="true" style="vertical-align:sub" class="dashicons dashicons-external"></span> WordPress Guide </a>
            <a class="button button-secondary" href="?ziegenhagel_sync" title="Sync with Ziegenhagel Server & force Plugin Update" style=float:right;display:flex;align-items:center><span class="dashicons dashicons-image-rotate"></span></a>
        </div>


        <?php
    }
}
if (!function_exists('ziegenhagel_wp_dashboard_care')) {
    function ziegenhagel_wp_dashboard_care()
    {

	if(!current_user_can( 'edit_posts' )){echo "Keine Erlaubnis dieses Widget anzuzeigen.";return;}
        ?>

        <div style="margin:10px auto;text-alin:center ">Unser Rundum-Service für Ihre Webseite!
            <a href="https://www.ziegenhagel.com/services/careplus" target="_blank">Mehr erfahren</a>
        </div>

        <span class="careicon careicon-<?php echo get_option("ziegenhagel_care", ""); ?>"><?php echo get_option("ziegenhagel_care", ""); ?> Paket</span>
        <table width="100%">
            <?php
            $plans = json_decode(get_option("ziegenhagel_care_plans", "[]"), 1);
            if (is_array($plans)) foreach ($plans as $plan) {
                if (substr($plan["Titel"], 0, 5) === "Preis" && !strstr(@$plan[get_option("ziegenhagel_care", "")], "//")) continue;
                echo "<tr><td style='background:#ddd;padding:.3em'>" . $plan["Titel"] . "</td><td style='text-align:right;background:#eee;padding:.3em'>" . (@$plan[get_option("ziegenhagel_care", "")] == "" ? "<span style=opacity:.4>Nein</span>" : "<div>" . str_replace("/", " ", @$plan[get_option("ziegenhagel_care", "")])) . "</div></td></tr>";
            } else echo "Tabelle ausgeblendet.";
            ?>
        </table>
        <div style=margin-top:10px>
            <?php
            if (get_option("ziegenhagel_dev", false)) echo "<span style=float:right;opacity:.5>-dev</span> "; // don't do updates when developing

            // file m time
            echo "<span style=float:right;opacity:.5>-".date("ymd",(filemtime(__FILE__)))."</span>";

            $plugin_data = get_file_data(__FILE__, array('Version' => 'Version'), false);
            $plugin_version = $plugin_data['Version'];
            echo "<span style=float:right;opacity:.5>Version " . $plugin_version . "</span>";
            ?>
            <a href="https://www.ziegenhagel.com/services/careplus" target="_blank">Alle Pläne ansehen</a>
        </div>
        <?php
    }
}

if (!function_exists('folderSize')) {
    function folderSize($dir, $echo = false)
    {
        $size = 0;

        // if($dir == "../") $size = 0; // fake storage usage in GB

        foreach (glob(rtrim($dir, '/') . '/*', GLOB_NOSORT) as $each) {
            $size += is_file($each) ? filesize($each) : folderSize($each, $echo);

            if ($echo) {

                // display files bigger than 100MB with their size in MB
                echo filesize($each) > 100000000 ? "<li><b>" . round(filesize($each) / 1000000, 2) . 'MB</b><br>' . substr($each, 3) . '</li>' . PHP_EOL : '';

            }
        }

        return $size;
    }
}

// if not defined yet, define a maximum storage size of 50GB
if (!defined('ZIEGENHAGEL_EMAIL_STORAGE')) define('ZIEGENHAGEL_EMAIL_STORAGE', 5000000000); // in bytes
if (!defined('ZIEGENHAGEL_MAX_STORAGE')) define('ZIEGENHAGEL_MAX_STORAGE', 20000000000 + ZIEGENHAGEL_EMAIL_STORAGE); // in bytes

if (!function_exists('ziegenhagel_wp_dashboard_hosting')) {
    function ziegenhagel_wp_dashboard_hosting()
    {

	if(!current_user_can( 'edit_posts' )){echo "Keine Erlaubnis Support anzuzeigen.";return;}

        echo "<b>Speichernutzung:</b></p><details style='margin-top:-6px;margin-bottom:7px;'><summary style='cursor:pointer;'>Große Dateien</summary><ul>";
        $used = folderSize("../", true);
        echo "</ul></details>";

        /* Display a progress bar */
        $percent =  round($used / ZIEGENHAGEL_MAX_STORAGE * 100) ;
        echo "<div style='width:100%;height:8px;border-radius:4px;background:#ddd;margin:10px 0;overflow: hidden;display:flex'>
            <div style='width:" .$percent. "%;height:100%;background:".($percent > 70 ?'red': '#2271b1').";'></div>
            <div style='width:".( ZIEGENHAGEL_EMAIL_STORAGE / ZIEGENHAGEL_MAX_STORAGE * 100)."%;height:100%;background:".($percent > 70 ?'orange': '#b1cee3').";'></div>
        </div>";
        echo "<div><span style='background:".($percent > 70 ?'red': '#2271b1').";display:inline-block;width:10px;height:10px;border-radius:50%;'></span> WordPress (".round($used / 1000 / 1000 / 1000 * 10) / 10 . "GB)"
                ."&emsp;<span style='background:".($percent > 70 ?'orange': '#b1cee3').";display:inline-block;width:10px;height:10px;border-radius:50%;'></span> E-Mail Postfach (".round(ZIEGENHAGEL_EMAIL_STORAGE / 1000 / 1000 / 1000 * 10) / 10 . "GB)</div>";

        // echo "Gesamt: ".round(ZIEGENHAGEL_MAX_STORAGE / 1000 / 1000 / 1000 * 10) / 10 . "GB";

    }
}

// setze das upload limit beim uploaden durch
if (!function_exists('ziegenhagel_no_upload_after_limit')) {
    function ziegenhagel_no_upload_after_limit()
    {

        // allow a few pages
        $allowed_pages = ["/wp-login.php", "/wp-admin/index.php", "/wp-admin/admin-ajax.php", "/wp-admin/upload.php"];
        if (in_array($_SERVER["REQUEST_URI"], $allowed_pages) && !count($_FILES)) return;

        // if not logged in return
        if (!is_user_logged_in()) return;

        // if the user is trying to upload a file, that would exceed the maximum storage size, don't allow it
        $used = folderSize("../") + ZIEGENHAGEL_EMAIL_STORAGE;
        if (isset($_FILES["async-upload"]) && $used + $_FILES["async-upload"]['size'] > ZIEGENHAGEL_MAX_STORAGE) {

            ziegenhagel_head();

            $out = '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
  <path stroke-linecap="round" stroke-linejoin="round" d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0M3.124 7.5A8.969 8.969 0 015.292 3m13.416 0a8.969 8.969 0 012.168 4.5" />
</svg> ';
            $out .= "<h1>Speicherplatz Limit überschritten</h1>";
            $out .= "<p>Sie haben bereits " . round($used / 1000 / 1000 / 1000 * 10) / 10 . "GB von " . round(ZIEGENHAGEL_MAX_STORAGE / 1000 / 1000 / 1000 * 10) / 10 . "GB Speicherplatz genutzt. Bitte löschen Sie Dateien, um weiter hochladen zu können.</p>";

            // show big files
            $out .= "<details><summary>Große Dateien</summary><ul>";
            echo $out;
            folderSize("../", true);
            $out = "</ul></details>";

            $out .= "<p><a class=btn href='/wp-admin/upload.php'>Zur Dateiverwaltung</a></p>";

            die($out);
        }
    }

    // if max storage is defined, add the function to the upload hook
    if (defined('ZIEGENHAGEL_MAX_STORAGE'))
        add_action('init', 'ziegenhagel_no_upload_after_limit');
}

if (!function_exists('ziegenhagel_head')) {
    function ziegenhagel_head() {
        wp_head();

        ?>
        <style>
       #container {
       max-width:500px;
       width:100%;
        margin: 50px auto;
        border: 2px solid #0003;
        padding: 10px 30px;
        text-align: center;
        border-radius:5px;
       }

       ul {
       text-align: left;
       }

       ul li {
       list-style: none;
       display:block;
       line-height: 1.4em;
       padding: 5px 10px;
       border-top: 1px solid #0005;
       }
       
       details summary {
       cursor:pointer;
       }

      ul  li:hover {
       background: #0001;
       }

       body{
           font-family: sans-serif;
            color:#333;
       }

       svg {
       height:70px;
       margin-top: 20px;
       }

       p {margin:2em 0;}

       a.btn {
            border: 2px solid #333;
            padding: .4em .8em;
            border-radius: 8px;
            text-decoration: none;
            color:#333;
            display: inline-block;
            font-weight: 600;
       }
       a.btn::after {content:" >";}
       a.btn:hover {
       background:#0001;
       }
</style>
        <div id="container">
        <?php
    }

}
/*
print_r($_FILES);
Array
(
    [async-upload] => Array
        (
            [name] => img_0005-thumb.jpg
            [type] => image/jpeg
            [tmp_name] => /tmp/phpYwJ3XO
            [error] => 0
            [size] => 55544
        )

)
*/

if (!function_exists('ziegenhagel_wp_dashboard_invoices')) {
    function ziegenhagel_wp_dashboard_invoices()
    {

	if(!current_user_can( 'edit_posts' )){echo "Keine Erlaubnis Support anzuzeigen.";return;}

        $status = ["not_paid" => "Zahlung ausbleibend", "paid" => "Zahlung abgeschlossen", "awaiting_payment" => "Zahlung ausstehend"];
        $color = ["not_paid" => "#ddd;text-decoration:line-through", "paid" => "#e5ff8c", "awaiting_payment" => "#ffe67c"];
        ?>
        <table width="100%">
            <?php
            $invoices = unserialize(get_option("ziegenhagel_invoices", 's:2:"[]"'));
            if ($invoices == []) {
                echo "Keine Rechnungen gefunden.";
            } else {
                foreach ($invoices as $invoice) {
                    echo "<tr style=background:" . $color[$invoice["status"]] . ">
                     <td>R" . $invoice["invoice_no"] . "</td>
                     <td>" . $status[$invoice["status"]] . "</td>
                     <td>" . date("d.m.Y", strtotime($invoice["iss"])) . "</td>
                     <td><a href='https://api.ziegenhagel.com/pdf/?pid=" . $invoice["pid"] . "&id=" . $invoice["id"] . "&type=invoice&secret=" . substr(get_option("ziegenhagel_project_secret", ""), 0, 16) . "' target=_blank>PDF</a></td></tr>";
                }
            }
            ?>
        </table>
        <?php
    }
}
if (!function_exists('ziegenhagel_wp_dashboard_appointments')) {
    function ziegenhagel_wp_dashboard_appointments()
    {

	if(!current_user_can( 'edit_posts' )){echo "Keine Erlaubnis dieses Widget anzuzeigen.";return;}
        ?>
        <p style=margin-bottom:0>Bevorstehend:</p>
        <table width="100%">
            <?php
            $termine = unserialize(get_option("ziegenhagel_appointments", 's:2:"[]"'));
            $f = 0;
            if ($termine) {
                $termine = array_reverse($termine);
            }
            foreach ($termine as $termin) {
                if (strtotime($termin["datetime"]) >= time()) $f++;
            }
            if ($f == 0) echo "<span style=opacity:.5>Keine bevorstehenden Termine.</span>";
            $i = 0;
            foreach ($termine as $termin) {
                if ($i >= 5) break;
                if (strtotime($termin["datetime"]) < time() && $i == 0) {
                    echo "</table><p style=margin-top:15px;margin-bottom:0>Vergangen:</p><table width='100%'>";
                }
                echo "<tr";
                if (strtotime($termin["datetime"]) < time()) {
                    $i++;
                    echo " style=opacity:.4";
                }
                echo "><td width='100' style='background:#ddd;padding:.3em'>" . date("d.m.Y H:i", strtotime($termin["datetime"])) . " Uhr</td><td style='text-align:left;background:#eee;padding:.3em'>" . $termin["topic"] . "</td></tr>";
            }
            ?>
        </table>
        <?php
    }
}

if (!function_exists('ziegenhagel_wp_dashboard_tip')) {
    function ziegenhagel_wp_dashboard_tip() {
        global $schon_gewusst;
        shuffle($schon_gewusst);
        echo "<div style='display:flex;'>";
        echo "<div style='flex:1'><b>" . $schon_gewusst[0]["title"] . "</b>";
        echo "<p>" . $schon_gewusst[0]["description"] . "</p></div>";
        echo '<svg style="min-width:50px;margin:20px;flex:0;" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.2" stroke="currentColor" class="w-6 h-6"> <path stroke-linecap="round" stroke-linejoin="round" d="M12 18v-5.25m0 0a6.01 6.01 0 001.5-.189m-1.5.189a6.01 6.01 0 01-1.5-.189m3.75 7.478a12.06 12.06 0 01-4.5 0m3.75 2.383a14.406 14.406 0 01-3 0M14.25 18v-.192c0-.983.658-1.823 1.508-2.316a7.5 7.5 0 10-7.517 0c.85.493 1.509 1.333 1.509 2.316V18" /> </svg>';
        echo "</div>";
    }
}
if (!function_exists('ziegenhagel_wp_dashboard_setup')) {
    function ziegenhagel_wp_dashboard_setup()
    {

        if(!current_user_can( 'edit_posts' )){return;}

        wp_add_dashboard_widget('ziegenhagel_wp_dashboard_support', __('Übersicht'), 'ziegenhagel_wp_dashboard_support');
        wp_add_dashboard_widget( 'ziegenhagel_wp_dashboard_tip', __( 'Tipp' ), 'ziegenhagel_wp_dashboard_tip' );

        wp_add_dashboard_widget('ziegenhagel_wp_dashboard_care', __('Care+'), 'ziegenhagel_wp_dashboard_care');
        wp_add_dashboard_widget('ziegenhagel_wp_dashboard_invoices', __('Rechnungen'), 'ziegenhagel_wp_dashboard_invoices');

        if (defined('ZIEGENHAGEL_MAX_STORAGE')) {
            wp_add_dashboard_widget('ziegenhagel_wp_dashboard_hosting', __('Hosting'), 'ziegenhagel_wp_dashboard_hosting');
        }

        if(is_ziegenhagel_careplus_admin() && (get_option("ziegenhagel_console", "0") == "1" || isset($_GET["ziegenhagel_console"]))){
            wp_add_dashboard_widget('ziegenhagel_wp_dashboard_console', __('Console'), 'ziegenhagel_wp_dashboard_console');
        }

        //wp_add_dashboard_widget( 'ziegenhagel_wp_dashboard_appointments', __( 'Ziegenhagel Termine' ),'ziegenhagel_wp_dashboard_appointments');
    }
}
add_action('wp_dashboard_setup', 'ziegenhagel_wp_dashboard_setup');

if (!function_exists('modify_admin_footer')) {
    function modify_admin_footer()
    {
        echo '<span id="footer-thankyou">Danke für Ihre Vertrauen in <a href="https://www.ziegenhagel.com" target="_blank" rel="noopener noreferrer">Ziegenhagel Media</a>.</span>';
    }
}
add_filter('admin_footer_text', 'modify_admin_footer');


if (!function_exists('modify_admin_toolbar')) {
    function modify_admin_toolbar($admin_bar)
    {
// admin toolbar first level item
        $admin_bar->add_menu(array(
            'id' => 'quick-links',
            'title' => '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADEAAAAyCAYAAAD1CDOyAAAGMGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS41LjAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iCiAgICB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iCiAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIgogICAgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIKICAgZXhpZjpDb2xvclNwYWNlPSI2NTUzNSIKICAgZXhpZjpQaXhlbFhEaW1lbnNpb249IjQ5IgogICBleGlmOlBpeGVsWURpbWVuc2lvbj0iNTAiCiAgIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiCiAgIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJEaXNwbGF5IgogICB0aWZmOkltYWdlTGVuZ3RoPSI1MCIKICAgdGlmZjpJbWFnZVdpZHRoPSI0OSIKICAgdGlmZjpSZXNvbHV0aW9uVW5pdD0iMiIKICAgdGlmZjpYUmVzb2x1dGlvbj0iMTQ0LjAiCiAgIHRpZmY6WVJlc29sdXRpb249IjE0NC4wIgogICB4bXA6TWV0YWRhdGFEYXRlPSIyMDIwLTExLTExVDIyOjUwOjEwKzAxOjAwIgogICB4bXA6TW9kaWZ5RGF0ZT0iMjAyMC0xMS0xMVQyMjo1MDoxMCswMTowMCI+CiAgIDx0aWZmOkJpdHNQZXJTYW1wbGU+CiAgICA8cmRmOlNlcT4KICAgICA8cmRmOmxpPjg8L3JkZjpsaT4KICAgIDwvcmRmOlNlcT4KICAgPC90aWZmOkJpdHNQZXJTYW1wbGU+CiAgIDx0aWZmOllDYkNyU3ViU2FtcGxpbmc+CiAgICA8cmRmOlNlcT4KICAgICA8cmRmOmxpPjE8L3JkZjpsaT4KICAgICA8cmRmOmxpPjE8L3JkZjpsaT4KICAgIDwvcmRmOlNlcT4KICAgPC90aWZmOllDYkNyU3ViU2FtcGxpbmc+CiAgIDx4bXBNTTpIaXN0b3J5PgogICAgPHJkZjpTZXE+CiAgICAgPHJkZjpsaQogICAgICB4bXBNTTphY3Rpb249InByb2R1Y2VkIgogICAgICB4bXBNTTpzb2Z0d2FyZUFnZW50PSJBZmZpbml0eSBQaG90byAoSnVsIDMwIDIwMjApIgogICAgICB4bXBNTTp3aGVuPSIyMDIwLTEwLTIxVDIxOjQ0OjAyKzAyOjAwIi8+CiAgICAgPHJkZjpsaQogICAgICBzdEV2dDphY3Rpb249InByb2R1Y2VkIgogICAgICBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZmZpbml0eSBQaG90byAoSnVsIDMwIDIwMjApIgogICAgICBzdEV2dDp3aGVuPSIyMDIwLTExLTExVDIyOjUwOjEwKzAxOjAwIi8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICA8L3JkZjpEZXNjcmlwdGlvbj4KIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+Cjw/eHBhY2tldCBlbmQ9InIiPz7lfkCBAAAMbGlDQ1BEaXNwbGF5AABIiaVXd1xTyRaeW1JJaIEISAm9iSI1gJQQWgQBqYKohCSQUGJMCCJ2dFGBtYsoVnRVRNG1ALJW7GVR7P1hQUVZFws2VN6EBHT1vffPm99v7nw5c+Y75Z65mQFApytXkKdAdQHIk+bL48KDWWNSUlmkx4ACBgMioANrvkAh48TGRgHY+sd/tnfXAaIar7iouH6e/59NXyhSCABA0iDOFyoEeRBfAgAvFsjk+QAQE6DcenK+TIULITaQQwchLlfhLDXeoMIZaryvTychjgvxGQDIND5fngWA9lUoZxUIsiCP9luIXaVCiRQAHRuIAwRivhBi2MGQvLyJKrwYYgeoL4O4EWJ2xnecWf/gzxjg5/OzBrA6rr5GDpEoZLn8Kf0xk0EIkAAFkIFcwAcD4v+/5eUq+23awU4TyyPiVPmAOb2ZMzFShWkQd0ozomNUuYf4g0Sofg8AoFSxMiJRrY+aChRcmE/AhNhVyA+JhNgU4jBpbnSURp6RKQnjQQyrBy2U5PMSNGvnixSh8RrONfKJcTH9OFPO5WjW1vHlfXZV+ieUOYkcDf9NsYjXz/+2SJyQDDEVAIxaIEmKhlgbYgNFTnykWgezKhJzo/t15Mo4lf/wnWJskTQ8WM2PpWXKw+I0+jJNhUJ/sBKxhBetwZX54oQIdX6w7QJ+n/9GENeLpJzEfh6RYkxUfyxCUUioOnasRSRN1MSL3ZflB8dp1nbJcmM1+jhZlBuukltBbKIoiNesxUfkw2JV8+NRsvzYBLWfeHo2f2Ss2h+8AEQBLqwZFlDCngEmgmwgaels6IS/1DNhsI7kIAuIgItG0r8iuW9GCp/xoAj8BZEIVl7/uuC+WREogPIvA1L10wVk9s0W9K3IAU8gzgORsGZF0A/VKumAtSTwGEokP1kXQF9zYVfN/SzjQEmURqLs52Xp9GsSQ4khxAhiGNERN8EDcD88Cj6DYHfD2bhPv7ff9AlPCK2Eh4RrhDbCrQmSYvkPvowCbZA/TBNxxvcR43aQ0xMPxv0hO2TGmbgJcME9oB0OHggte0IpV+O3KnbWf4hzIILvcq7Ro7hSUMogShDF4ceV2k7angMsqox+nx+1rxkDWeUOzPxon/tdnoVwjPxRE5uP7cVOY8ews9hBrAGwsCNYI3YBO6TCAzX0uK+G+q3F9fmTA3kkP9nja2yqMqlwrXXtcP2smQP5osJ81QbjTpRNkUuyxPksjkyWK2LxpIKhQ1hurm6uAKj+Y9SfqTfMvv8OhHnum2yOJQD+U3p7ew9+k0XC/4S9h+A2v/1NZt8OPwfnADizUqCUF6hluOpBgF8DHbijjIE5sAYOMCI34AX8QBAIBSNBDEgAKWA8zLMY1rMcTAbTwGxQAsrAYrACrAbrwSawDewEe0ADOAiOgVPgPLgEroE7sH7awQvQBd6BHgRBSAgdYSDGiAViizgjbggbCUBCkSgkDklB0pEsRIookWnIHKQMWYqsRjYiNcjvyAHkGHIWaUVuIQ+QDuQ18gnFUBpqgJqhdugwlI1y0Eg0AR2HZqGT0CJ0LroQrUSr0R1oPXoMPY9eQ9vQF2g3BjAtjIlZYi4YG+NiMVgqlonJsRlYKVaBVWN1WBN801ewNqwT+4gTcQbOwl1gDUfgibgAn4TPwMvx1fg2vB4/gV/BH+Bd+FcCnWBKcCb4EniEMYQswmRCCaGCsIWwn3AS7qZ2wjsikcgk2hO94W5MIWYTpxLLiWuJu4hHia3ER8RuEolkTHIm+ZNiSHxSPqmEtIq0g3SEdJnUTvpA1iJbkN3IYeRUspRcTK4gbycfJl8mPyX3UHQpthRfSgxFSJlCWUTZTGmiXKS0U3qoelR7qj81gZpNnU2tpNZRT1LvUt9oaWlZaflojdaSaM3SqtTarXVG64HWR5o+zYnGpaXRlLSFtK20o7RbtDd0Ot2OHkRPpefTF9Jr6Mfp9+kftBnaQ7V52kLtmdpV2vXal7Vf6lB0bHU4OuN1inQqdPbqXNTp1KXo2ulydfm6M3SrdA/o3tDt1mPoDdeL0cvTK9fbrndW75k+Sd9OP1RfqD9Xf5P+cf1HDIxhzeAyBIw5jM2Mk4x2A6KBvQHPINugzGCnQYtBl6G+oYdhkmGhYZXhIcM2Jsa0Y/KYucxFzD3M68xPg8wGcQaJBi0YVDfo8qD3RoONgoxERqVGu4yuGX0yZhmHGucYLzFuML5ngps4mYw2mWyyzuSkSedgg8F+gwWDSwfvGXzbFDV1Mo0znWq6yfSCabeZuVm4mcxsldlxs05zpnmQebb5cvPD5h0WDIsAC4nFcosjFs9ZhiwOK5dVyTrB6rI0tYywVFputGyx7LGyt0q0KrbaZXXPmmrNts60Xm7dbN1lY2EzymaaTa3NbVuKLdtWbLvS9rTtezt7u2S7eXYNds/sjex59kX2tfZ3HegOgQ6THKodrjoSHdmOOY5rHS85oU6eTmKnKqeLzqizl7PEea1z6xDCEJ8h0iHVQ2640Fw4LgUutS4PhjKHRg0tHtow9OUwm2Gpw5YMOz3sq6una67rZtc7w/WHjxxePLxp+Gs3JzeBW5XbVXe6e5j7TPdG91cezh4ij3UeNz0ZnqM853k2e37x8vaSe9V5dXjbeKd7r/G+wTZgx7LL2Wd8CD7BPjN9Dvp89PXyzffd4/u3n4tfjt92v2cj7EeIRmwe8cjfyp/vv9G/LYAVkB6wIaAt0DKQH1gd+DDIOkgYtCXoKceRk83ZwXkZ7BosD94f/J7ry53OPRqChYSHlIa0hOqHJoauDr0fZhWWFVYb1hXuGT41/GgEISIyYknEDZ4ZT8Cr4XWN9B45feSJSFpkfOTqyIdRTlHyqKZR6KiRo5aNuhttGy2NbogBMbyYZTH3Yu1jJ8X+MZo4OnZ01egnccPjpsWdjmfET4jfHv8uIThhUcKdRIdEZWJzkk5SWlJN0vvkkOSlyW1jho2ZPuZ8ikmKJKUxlZSalLoltXts6NgVY9vTPNNK0q6Psx9XOO7seJPxueMPTdCZwJ+wN52Qnpy+Pf0zP4Zfze/O4GWsyegScAUrBS+EQcLlwg6Rv2ip6Gmmf+bSzGdZ/lnLsjrEgeIKcaeEK1kteZUdkb0++31OTM7WnN7c5NxdeeS89LwDUn1pjvTERPOJhRNbZc6yElnbJN9JKyZ1ySPlWxSIYpyiMd8AHuYvKB2UvygfFAQUVBV8mJw0eW+hXqG08MIUpykLpjwtCiv6bSo+VTC1eZrltNnTHkznTN84A5mRMaN5pvXMuTPbZ4XP2jabOjtn9p/FrsVLi9/OSZ7TNNds7qy5j34J/6W2RLtEXnJjnt+89fPx+ZL5LQvcF6xa8LVUWHquzLWsouxzuaD83K/Df638tXdh5sKWRV6L1i0mLpYuvr4kcMm2pXpLi5Y+WjZqWf1y1vLS5W9XTFhxtsKjYv1K6krlyrbKqMrGVTarFq/6vFq8+lpVcNWuNaZrFqx5v1a49vK6oHV1683Wl63/tEGy4ebG8I311XbVFZuImwo2PdmctPn0b+zfaraYbCnb8mWrdGvbtrhtJ2q8a2q2m25fVIvWKms7dqTtuLQzZGdjnUvdxl3MXWW7wW7l7ue/p/9+fU/knua97L11+2z3rdnP2F9aj9RPqe9qEDe0NaY0th4YeaC5ya9p/x9D/9h60PJg1SHDQ4sOUw/PPdx7pOhI91HZ0c5jWcceNU9ovnN8zPGrJ0afaDkZefLMqbBTx09zTh8543/m4FnfswfOsc81nPc6X3/B88L+Pz3/3N/i1VJ/0fti4yWfS02tI1oPXw68fOxKyJVTV3lXz1+LvtZ6PfH6zRtpN9puCm8+u5V769Xtgts9d2bdJdwtvad7r+K+6f3qfzn+a1ebV9uhByEPLjyMf3jnkeDRi8eKx5/b5z6hP6l4avG05pnbs4MdYR2Xno993v5C9qKns+Qvvb/WvHR4ue/voL8vdI3pan8lf9X7uvyN8Zutbz3eNnfHdt9/l/eu533pB+MP2z6yP57+lPzpac/kz6TPlV8cvzR9jfx6tzevt1fGl/P7jgIY7GhmJgCvtwJATwGAAc8Q1LHqO2BfQ9T31j4E/htW3xP7mhcAdXBQHde5RwHYDbvdLMgNR9VRPSEIoO7uA13TFJnubmouGrzxED709r4xA4DUBMAXeW9vz9re3i+bobO3ADg6SX33VDUivBtsCFGhW8vGzQI/NPW99LsYfxyBygMP8OP4b2rtjtpa0kBlAAAACXBIWXMAABYlAAAWJQFJUiTwAAAIm0lEQVRogbWae4zdVRHHP+f2duljKaWUPhIKAgKtgISWkCoQNUolQASJBF9BlIiAEVGEgBJNwMSYYCWgIFGKGEGLoFHQxBgDFcVqsRGr0JaHtNDClkK37W67tLv34x9zfu7d2/vcXiY56fb3+505850zc2bOzE3sB6kJSMCBeUwHFgLvAOYBBwM9wA5gA/AM8BgwmFnsAAZTSpX9kaM8TsHJwh0PLAJOA04FjiVAJUBgLfAU8CSwmgAxG3gg81gFrFRX5W/3AqSUHB+c9kGU1JPVu9T/qLscS0PqY+ql6knqdHVCnjtLfajm+0F1jXpn/r70Vgme8uhVl6rDNYJU1BF1k3qeus8Oq2X1ijqgq2k48z+4WLObACarH1JvU7dkoQsaUdepN6pTGy2uzlNXV80dybvwuvpaHlvUrerj6rnqAe0AaeoTmcEc4GrgU8DT+VXBeBh4EPgesCqltKcJuw8De4D7gE157MxjhPChEjCROCBmAkcCW9Rt4/ITw/bnqverb6q71UeyplR3qNeqM1rwSeoEwx/mqNNaaTh/P0f9g3p3ntu5rxgO+ZMq+9+u3le17ZdnYRoyz37wAXVuB+uW1GPUP2eTG1Z/oB7YifBJna3eW2P7/eoP1ZXqx9WJrexVXWKcPIs7WPsw9ZeOPTyGjdPrkJY+4ugJ9J2shWrqV29QT83aamYOJXWx4fAV9aI2QZTUe7L5VlMlA7lZndQUSN7+C9Vt7kv9eQdaHn3qKeorVXO/3CaIq+oor5r61LNr16+15wOJE6evzhoJKKWUrHdSZHAldQlwNxGZC+ptIXxSFwE31JGpmmYBXyFOrn1BZHRXA2cBdwH9xLFXUA8Bsq4QwCTgs8D3gXcyegxT83c96gUurRWuAZ0BXLaPSWdNnKwOGBH199msttTY5fUNQByq3p5Nrh5d00iivPZCdWMTM6qminG8zy9AFDsxAbgKmApMBpYAtwJ3As8DlazNueqkKgGmqCcRCd3ngYMayPpqE82WgPOIrHcMvjx2EwFxG/A6sCX//8IsE+WM5gTg9Bomc4HLiUzzdWB+fjZZHc5zLgIuI1LuRiazF3ixCYge4KNZ4JS/35yFl0jb9wID+e83iBR+dwa+oUzswunAYTXME+FI7ydShNXAEGHv5+RxDJEmNKO+rIRG9C7gaELTfyRSkPnADCItOoKwjlolDQL96rIy4VSnEY5ZTYVTHwC8jdDOEcBvgSl1mDaidcD2Ju8vAJYCzwLXEIppJ8WYmuV+oEygnJWFrGThy3kMEfYHsDg/6yQ9rhCXom31Xqo9wM+BK4EvEArrhP8pwLQCxD+AV4BDiGNuCrEz0wkzG29e3w+sTCkN1QFwEHAJcaAcOc413g7MLgPnAp8hbLuHzrXRjDYBK2ofqhOBbxBxZep+rDcJWFgmHHXGfjBqRAI/Bl4b8zCy0e8SiuvGmieVgOO6wKiWBP4F3DPmYdw9vklE524AEDiqRPMzfryMdwLXA/1FnpWd+BPEDbGbNKdEnEzdpmXAippE8Sjg68C0Lq6TgN4yo+G9W7uxArgjpbQb/p8c9gK3E6dft31vpARs7RIziULCV4Hnat59koj8bwUNlIlo2g3trCFyqfU1z+cS+VW3d6CgzWWifLh4nItI5Dp/B64A1lX7gVFEWEL4QzdoD/AmkRDuJTKCNWXCBMYLYCvwEJH71JoQRCA7gwaXqRa8Uxayj4j8w8AuIosdIFLyAWB5Gfgroc0JHSwyAjxO3Dd2A883KG4dShSaO1XSTiLO7CGKd1MysNnAiYxmtXuAB8vAy0TZvdmWW/XveuBb+e+vARtSSg83mDcLWNCm4BLavpdIGq8kMtoJNFbC80BfmdiqPxHH3+Q8qUJoeIhAWyGy3DWZ4U3A4XnhbzcRbDHN0+pCOS8AjwC3ARuBG/M6a/M6vYy2DKppFbC9TNjV/VkLhxJJ4GQiESySwUp+d3F+VtAm4HdNhDyhBYA3gOXAz4Ani2xX3ZyFrwAvEcnpUTUgBglXGCynlEbUPsLWCpMqZ2F7aFx0rhB3650N3kPc0OqZQoUwy2uBR1NKgzXv+wgnnkmk2+uBXxFF6WJHNgNPpJRGyMgnqr9os9pQ0GZzNbARAvWfdeYNGYXiwxvNVd+rvlw1Z0Rdr/7IqAmrLqutdgwT1Y1djK01NaJh4NfAMy36bbX379eAW4DzgZebzN1O+GJBJcLJjyUy4xcI/xlLRjl9qWOLyPWoor6kLrJ1OfOpqjn96ueMWm+refOz5uvt4jL1PQ0twKg6P90CxIj66Qy6lTCr85y96kes0wZrMO9Yoxhdj1aq05tNLhtF460NGAwau9VWYFQfNIrAl7TzfdW84xqAeFW9oOEu5MnJaK4szRovTKtidIruVme22oEqftcZ7YApbX6f1AXqiepzVcJX8rjJNsyxYDRTfdixTcLlRvur7RTCaJi0fQlSv2g0d05zbG12RP2pelAn66P2qL9R3zCKxT1tT+6ADN+aZvT/BtQV6lnZdDQaLg/Ypj/VMi+pRxh96aOzFprFhFIW5kgjfrQ0obzrRxut5R155x9VLzbiwZtG52hes7UbokspVdSNRPQ8h0g5dqtbiVRlLxE5JxIp90yi2DYd+C9R0dvVBEAPcDbRNDmV0ZiynbhIAdxM3Ndf2e+fShgR/fxsp8N5jFSN6tgyYMSDfQrNjraDJ6t3qDsdSxUjDlynnjkuE2oComhlHW+0hTfWEb4Q4hGjB52q5iajXbxA/ZKRslQce/oNqy+qHzM3F+3EiTsENEn9YLbjZ2sE2ZD9oQBQyoDOU281IvieOsDXqreo76u3g61oXEizkFOIW9fpRIFgAVEY+wth0+8GziTuFHPZt0g3QJR3lgNPEAXtofHY/n5tl6M/2ip+lzGDaF0tIX641ZufS1y+1gH/Ji4zf8vPAOp2ZNul/wHZ4vchsjio7AAAAABJRU5ErkJggg==" style="height:19px;margin-bottom:-5px"> Ziegenhagel Support',
            'href' => ziegenhagel_mailto(),
            'meta' => array(
                'title' => __('Ziegenhagel Support'),
            ),
        ));

        // send mail with question to this side
        $admin_bar->add_menu(array(
            'id' => 'quick-link-three',
            'parent' => 'quick-links',
            'title' => 'Frage zu dieser Seite',
            'href' => ziegenhagel_mailto(),
            'meta' => array(
                'target' => '_blank',
                'title' => __('Frage zu dieser Seite'),
                'class' => 'quick-links-class'
            ),
        ));

// admin bar second level item
        $admin_bar->add_menu(array(
            'id' => 'quick-link-two',
            'parent' => 'quick-links',
            'title' => 'Termin vereinbaren',
            'href' => "https://go.ziegenhagel.com/termin",
            'meta' => array(
                'target' => '_blank',
                'title' => __('Termin vereinbaren'),
                'class' => 'quick-links-class'
            ),
        ));
// admin bar second level item
        $admin_bar->add_menu(array(
            'id' => 'quick-link-one',
            'parent' => 'quick-links',
            'title' => 'WordPress Guide',
            'href' => "https://ziegenhagel.com/support#wordpress-guide",
            'meta' => array(
                'target' => '_blank',
                'title' => __('WordPress Support'),
                'class' => 'quick-links-class'
            ),
        ));
    }
}
add_action('admin_bar_menu', 'modify_admin_toolbar', 100);


if (!function_exists('modify_admin_logo')) {
    function modify_admin_logo()
    { ?>
        <style type="text/css">
            #login h1 a, .login h1 a {
                background-image: url(<?php echo plugin_dir_url(__FILE__); ?>/logo.png);
                background-color: #9b2;
                animation: spinin .6s ease-out;
                height: 84px;
                width: 84px;
                background-size: 84px 84px;
                background-repeat: no-repeat;
                margin-bottom: 60px;
                border-radius: 42px;
            }

            @keyframes spinin {
                0% {
                    transform: rotate(-180deg) scale(.9);
                    opacity: 0
                }
                55% {
                    transform: rotate(-7deg);
                }
                100% {
                    transform: rotate(0deg);
                    opacity: 1
                }
            }
        </style>
    <?php }
}
add_action('login_enqueue_scripts', 'modify_admin_logo');


if (!function_exists('ziegenhagel_custom_css')) {
    function ziegenhagel_custom_css()
    { ?>
        <style type="text/css">
            :root {
                --primary: #580;
                --primaryLime: #580;
                --accent: #790;
                --fgtext: #fff;
            }

            /*
            a {color:var(--primary)}
            #adminmenu li a:focus div.wp-menu-image:before, #adminmenu li.opensub div.wp-menu-image:before, #adminmenu li:hover div.wp-menu-image:before
            {color:var(--primaryLime)}

            #adminmenu .wp-submenu a:focus, #adminmenu .wp-submenu a:hover, #adminmenu a:hover, #adminmenu li.menu-top>a:focus,
            #adminmenu li.menu-op:hover, #adminmenu li.opensub>a.menu-top, #adminmenu li>a.menu-top:focus,
            #adminmenu .wp-submenu a:focus * , #adminmenu .wp-submenu a:hover * , #adminmenu a:hover * , #adminmenu li.menu-top>a:focus * ,
            .wp-core-ui select:hover,
            .view-switch a.current:before,
            a:hover, a:active, a:focus {color:var(--accent)}

            .wp-core-ui .button, .wp-core-ui .button, .wp-core-ui .button, .wp-core-ui .button,
            .wp-core-ui .button-link, .wp-core-ui .button-link, .wp-core-ui .button-link,
            .wrap .add-new-h2:hover, .wrap .page-title-action:hover,
            .wrap .add-new-h2, .wrap .add-new-h2:active, .wrap .page-title-action, .wrap .page-title-action:active,
            a.button {border-color:var(--primaryLime);color:var(--primaryLime)}

            .wp-core-ui .button.focus, .wp-core-ui .button.hover, .wp-core-ui .button:focus, .wp-core-ui .button:hover,
            .wp-core-ui .button-link, .wp-core-ui .button-link, .wp-core-ui .button-link,
            a.button:hover {border-color:var(--accent);color:var(--accent)}

            .wp-core-ui .button-primary, .wp-core-ui .button-primary,
            .wp-core-ui .button-link:active, .wp-core-ui .button-link:focus, .wp-core-ui .button-link:hover,
            a.button.button-primary {background-color:var(--primaryLime);border-color:var(--accent);color:var(--fgtext)}

            .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover,
            a.button.button-primary:hover {background-color:var(--accent);border-color:var(--accent);color:var(--fgtext)}

            #adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head, #adminmenu .wp-menu-arrow, #adminmenu .wp-menu-arrow div, #adminmenu li.current a.menu-top, #adminmenu li.wp-has-current-submenu a.wp-has-current-submenu, .folded #adminmenu li.current.menu-top, .folded #adminmenu li.wp-has-current-submenu {
            background-color:var(--primaryLime);
            }
                */

            #dashboard-widgets-wrap table {
                border-collapse: collapse;
                border-spacing: 0;
            }

            #dashboard-widgets-wrap table tr td {
                border-top: 1px solid white;
                padding: 2px .5em !important
            }

            /* admin t3weaks */
            .nav-tab-wrapper #eum-premium,
            .nav-tab-wrapper #updraft-navtab-addons,
            table.wp-list-table.widefat.plugins .aWordfencePluginCallout,
            a.elementor-plugins-gopro,
            tr[data-slug="really-simple-ssl"] .row-actions > span:first-of-type,
            tr[data-slug="really-simple-ssl"] .row-actions > span:nth-of-type(2),
            tr[data-slug="wordpress-seo"] .row-actions > span:first-of-type,
            tr[data-slug="updraftplus"] .row-actions > span:first-of-type,
            div.yoast_premium_upsell,
            .wpseo_content_cell #sidebar,
            #easy-updates-manager-dashnotice,
            #updraft-dashnotice,
            .sfsi_new_prmium_follw.sfsi_banner_body,
            #wpbody div.notice {
                display: none !important;
            }

            <?php $current_user = wp_get_current_user(); if($current_user->user_login != "ziegenhagel" && $current_user->user_email != "info@ziegenhagel.com"  && $current_user->user_email != "wp@zghl.de" ) {?>
            /* hide for non admins */
            li.wp-has-submenu.wp-not-current-submenu.menu-top.menu-icon-plugins,
            li.wp-has-submenu.wp-not-current-submenu.menu-top.menu-icon-users,
            li.wp-has-submenu.wp-not-current-submenu.menu-top.toplevel_page_Wordfence,
            li.wp-has-submenu.wp-not-current-submenu.menu-top.toplevel_page_WP-Optimize,
            li.wp-has-submenu.wp-not-current-submenu.menu-top.toplevel_page_wpseo_dashboard,
            li.wp-has-submenu.wp-not-current-submenu.menu-top.toplevel_page_accessibility-settings,
            #adminmenu li.menu-top div.wp-menu-name span,
            .notice,
            tr[data-slug="ziegenhagel-care"] .row-actions > span:first-of-type,
            #wf-onboarding-plugin-header,
            #adminmenu .toplevel_page_elementor ul > li:last-of-type {
                display: none

            }

            <?php }?>
            .updraft-ad-container,
            #dashboard-widgets #wpseo-dashboard-overview, #welcome-panel,
            #dashboard-widgets #dashboard_quick_press,
            #dashboard-widgets #dashboard_primary,
            #dashboard-widgets #dashboard_activity,
            #dashboard-widgets #dashboard_right_now,
            #dashboard-widgets #dashboard_site_health,
            #menu-comments,
            #dashboard-widgets #wordfence_activity_report_widget,
            #dashboard-widgets #e-dashboard-overview,
            #wp-admin-bar-languages, #wp-admin-bar-comments, #wp-admin-bar-easy-updates-manager-admin-bar, #wp-admin-bar-updraft_admin_node, #wp-admin-bar-wpseo-menu {
                display: none
            }


            /* care plans */

            .careicon {
                background: #white;
                /*width:40px;                                                          */
                text-align: center;
                padding: .3em 0;
                display: flex;
                align-items: center;
                justify-content: center;
                color: #ddd;
                font-size: 1.4em;
            }

            .careicon-Silber {
                background: #d3dde0;
                color: gray;
            }

            .careicon-Gold {
                background: rgb(255, 206, 0);
                color: rgb(184, 119, 0);
            }

            .careicon-Diamant {
                background: rgb(29, 238, 183);
                color: rgb(14, 135, 88);
            }

            .careicon-Titan {
                background: #4e91a6;
                color: rgb(236, 250, 254);
            }
        </style>
    <?php }
}

add_action('admin_head', 'ziegenhagel_custom_css');


/* cron */

register_activation_hook(__FILE__, 'ziegenhagel_activation');
// register_deactivation_hook(__FILE__, 'ziegenhagel_activation');

if (!function_exists('ziegenhagel_activation')) {
    function ziegenhagel_activation()
    {
        if (!wp_next_scheduled('ziegenhagel_hourly_event')) {
            wp_schedule_event(time(), 'hourly', 'ziegenhagel_hourly_event');
        }
    }

}
add_action('ziegenhagel_hourly_event', 'ziegenhagel_do_this_hourly');

if (!function_exists('ziegenhagel_do_this_hourly')) {
    function ziegenhagel_do_this_hourly()
    {
        // do something every hour

        global $wp_version;
        $url = explode("/", str_replace("https://", "", str_replace("http://", "", get_site_url())));

        foreach (get_users() as $user) {
            $users[] = ["ID" => $user->ID, "user_login" => $user->user_login, "user_email" => $user->user_email, "roles" => $user->roles];
        }
        foreach (get_plugins() as $file => $plugin) {
            $plugins[] = ["title" => $plugin["Title"], "version" => $plugin["Version"]];
        }

        $data = [
            "wordpress_version" => $wp_version,
            "php_version" => phpversion(),
            "plugins" => $plugins,
            "theme" => wp_get_theme(),
            "users" => $users,
            "url" => $url[0],
            "secret" => get_option("ziegenhagel_project_secret", "")
        ];


        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, "https://api.ziegenhagel.com/wp_care.php");

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_POST, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        //echo($output);

        $data = json_decode($output, 1);

        if(isset($data["care"])) {
            update_option("ziegenhagel_care", $data["care"]);
            update_option("ziegenhagel_invoices", serialize($data["invoices"]));
            update_option("ziegenhagel_care_plans", $data["care_plans"]);
            update_option("ziegenhagel_appointments", serialize($data["appointments"]));
        } else {
            echo "API Antwort fehlerhaft, Daten wurden nicht aktualisiert.<hr>";
        }

        // lets run upate / auto repair
        ziegenhagel_care_do_update(); // lets dont

    }
}

/* adminbar frontend */
if (!function_exists('ziegenhagel_adminbar')) {
    function ziegenhagel_adminbar()
    {
        wp_enqueue_style('adminbar.css', plugins_url('adminbar.css', __FILE__), false, '1.0.0', 'all');
    }
}
add_action('wp_enqueue_scripts', "ziegenhagel_adminbar");


if (!function_exists('ziegenhagel_care_do_update')) {
    function ziegenhagel_care_do_update()
    {


        if (get_option("ziegenhagel_dev", false)) return; // dont do updates when developing

        $update = json_decode(file_get_contents(__DIR__ . "/update.json"));
        foreach ($update->untrack as $untrack) {
            unlink(__DIR__ . "/" . $untrack);
        }
        foreach ($update->track as $track) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $update->origin . $track);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);

            if (
                strstr($output,"api.ziegenhagel.com") &&
                !strstr($output, "https://stat"."us.gitlab.com/") && (
                    !strstr($track, ".php") || (
                        substr_count($output, "(") == substr_count($output, ")") &&
                        substr_count($output, "{") == substr_count($output, "}") &&
                        substr_count($output, '"') % 2 == 0 && // " <- yes, we need this quote, otherwise our checks will fail in this very line
                        substr_count($output, "[") == substr_count($output, "]")
                    )
                )
            ) {
                file_put_contents(__DIR__ . "/" . $track, $output);
            } else if($track != "logo.png")  {
                echo "<div class='alert alert-danger'>WARNING for ".$track;

                // display why we think this is a syntax error, based on our substr_counts
                echo "<br>parenthesis: " . substr_count($output, "(") . " vs " . substr_count($output, ")");
                echo "<br>brackets: " . substr_count($output, "[") . " vs " . substr_count($output, "]");
                echo "<br>curly brackets: " . substr_count($output, "{") . " vs " . substr_count($output, "}");
                echo "<br>quotes: " . substr_count($output, "'"). " and ". substr_count($output, '"'); //"' <--- dont remove theese quotes!!!
                echo "<br>https://sta"."tus.gitlab.com/ in output: " . (strstr($output, "https://stat"."us.gitlab.com/") ? "yes" : "no");

                echo "<br><br>Not updating this file, because it seems to be a syntax error. Please check the file manually.</div>";

            }

        }
    }
}

//redirect non-users to the coming soon page
function ziegenhagel_maintenance_mode() {
    $mode = get_option('ziegenhagel_maintenance_mode');
    if (!is_user_logged_in()) {

        if($mode === "coming_soon" || $mode === "maintenance")  {

            if($mode === "coming_soon")  {
                $title = "In Arbeit";
                $text = "Die Seite befindet sich in Arbeit. Bitte besuchen Sie uns später wieder.";
            }
            elseif($mode === "maintenance")  {
                $title = "Wartungsmodus";
                $text = "Die Seite befindet sich gerade in der Wartung. Bitte besuchen Sie uns später wieder.";
            }

            ?>
<!DOCTYPE html>
<html>
<body>
    <div class="container">
        <h1><?php echo $title; ?></h1>
        <p><?php echo $text; ?></p>
        <a href='/wp-login.php' class="btn">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
        </svg>
        Einloggen</a>
    </div>
    <style>

    html {
        display: flex;
        align-items: center;
        justify-content: center;
        min-height: 30%;
    }

    .container {
        max-width: 800px;
        margin: 20px auto;
        padding: 5% 10%;
        border-radius: 20px;
        font-family: sans-serif;
    }

    h1 {
        font-size: 2em;
        margin-bottom: 0;
    }

    p {
        font-size: 1.2em;
        line-height: 1.5em;
        margin-bottom: 1em;
    }

    .btn {
        color: #000;
        border: 2px solid #444;
        padding: .4em .8em;
        border-radius: 8px;
        text-decoration: none;
        cursor:pointer;
        transition: 80ms ease-in;
        font-size: 1.1em;
        display: inline-flex;
        align-items: center;
        justify-content: center;
    }

    .btn svg {
        margin-right: 10px;
        flex:1;
        height:100%;
        width:1.1em;
        color:#000;
    }
    .btn:hover { background: #0001; }
    .btn:active { background: #0002; }
    </style>
</body>
</html>
            <?php
            die();
        }
    }
}

add_action('get_header', 'ziegenhagel_maintenance_mode');



/*
 * have the careplus wiki
 */

add_action( 'admin_notices', 'careplus_wiki_notice' );
function careplus_wiki_notice() {

    $url = $_SERVER['REQUEST_URI'];
    $user_may_edit = is_ziegenhagel_careplus_admin();

    // get the option
    $careplus_wiki = get_option("careplus_wiki", "{}");
    // decode it
    $careplus_wiki = json_decode($careplus_wiki, 1);
    // check if we have a wiki for this page
    $wiki_content = $careplus_wiki[$url] ?? false;

    /* if the admin with id 1 is logged in, and $_POST["careplus_wiki"] is set, then we save the value */
    if ($user_may_edit && isset($_POST["careplus_wiki"])) {
        // set the value
        $careplus_wiki[$url] = $_POST["careplus_wiki"];
        // encode it
        $careplus_wiki = json_encode($careplus_wiki);
        // save it
        update_option("careplus_wiki", $careplus_wiki);
        // add it
        $wiki_content = $_POST["careplus_wiki"];
        // display a notice success
        echo "<div class='notice notice-success is-dismissible' style='display:block !important;'><p>Wiki-Eintrag gespeichert.</p></div>";
    }

    if(isset($_POST["careplus_preview"]))
        $user_may_edit = false;

    if($wiki_content||( $user_may_edit && get_option("ziegenhagel_careplus_wiki_editor","0")=="1")){ ?>
        <div class="notice notice-info is-dismissible careplus-wiki" style="display:block !important;">
            <p><strong>CarePlus Wiki</strong></p>
            <?php if($user_may_edit) { ?>
            <form method="post">
            <p><textarea width="100%" name="careplus_wiki" style="width:100%;<?php if(isset($_POST["careplus_wiki"])) echo'height:300px;';?>" onchange="this.form.submit()" placeholder="Eine Beschreibung für <?php echo $url;?> hinzufügen. HTML erlaubt."><?php echo $wiki_content;?></textarea></p>
            </form>
            <div style="display:flex;height:1rem;margin-bottom:1.4rem;gap:.5em">
                <button class="button button-primary" name="careplus_preview">Speichern</button>
                <form method="post" style="margin-bottom:10px">
                    <input type="submit" class="button button" name="careplus_preview" value="Vorschau">
                </form>
            </div>
            <?php } else { ?>
            <p><?php echo $wiki_content; ?></p>
            <p><a href="<?php echo ziegenhagel_mailto();?>" class="button">Frage zu dieser Seite</a></p>
            <?php } ?>
        </div>
        <style>
        .careplus-wiki {
        padding-bottom: 5px;
        }
        .careplus-wiki p strong {
            padding: 10px 0;
            display: block;
        }
        .careplus-wiki p {
            white-space: pre-wrap;
            line-height: .6rem;
        }
        .careplus-wiki img {
            max-width: 100%;
            height: 50px;
            margin: 10px 0;
            border: 1px solid #ccc;
        }
        </style>
<?php }
}

