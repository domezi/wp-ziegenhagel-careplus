<?php 

	$data = [
		"secret"=>get_option("ziegenhagel_project_secret"),
		"action"=>"uninstall"
	];
	// create curl resource 
	$ch = curl_init(); 

	// set url 
	curl_setopt($ch, CURLOPT_URL, "https://api.ziegenhagel.com/wp_care.php");

	//return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_POSTFIELDS,  http_build_query($data));
	curl_setopt($ch, CURLOPT_POST, 1);

	// $output contains the output string 
	$output = curl_exec($ch); 

	// close curl resource to free up system resources 
	curl_close($ch);    

	delete_option( 'ziegenhagel_project_secret' ); 
	delete_option( 'ziegenhagel_invoices' );
	delete_option( 'ziegenhagel_appointments' );
	delete_option( 'ziegenhagel_care' );
	delete_option( 'ziegenhagel_care_plans' );


?>


